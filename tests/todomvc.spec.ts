import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('https://todomvc.com/examples/vue/');
});

const TODO_ITEMS = [
  'Complete the SQR Lab',
  'Start preparing the final presentation',
  'Be the coolest person in existence',
];

test.describe('New Todo', () => {
  test('should allow me to add todo items', async ({ page }) => {
    const todoInput = page.locator('.new-todo');
    // Create 1st todo.
    await todoInput.fill(TODO_ITEMS[0]);
    await todoInput.press('Enter');

    // Make sure the list only has one todo item.
    await expect(page.locator('.view label')).toHaveText(TODO_ITEMS[0]);

    // Create 2nd todo.
    await todoInput.fill(TODO_ITEMS[1]);
    await todoInput.press('Enter');

    // Make sure the list now has two todo items.
    await expect(page.locator('.view label')).toHaveText([
      TODO_ITEMS[0],
      TODO_ITEMS[1]
    ]);
  });
});

test.describe('Edit Item', () => {
  test('should allow me to edit an item', async ({ page }) => {
    // Fill all the todos
    for (const item of TODO_ITEMS) {
      await page.locator('.new-todo').fill(item);
      await page.locator('.new-todo').press('Enter');
    }

    // Replace the text content of the second one
    const todoItems = page.locator('.todo-list li');
    const secondTodo = todoItems.nth(1);
    await secondTodo.dblclick();
    const editBox = secondTodo.locator('.edit');
    await expect(editBox).toHaveValue(TODO_ITEMS[1]);
    const replacementText = 'Something else';
    await editBox.fill(replacementText);
    await editBox.press('Enter');

    // Explicitly assert the new text value.
    await expect(todoItems).toHaveText([
      TODO_ITEMS[0],
      replacementText,
      TODO_ITEMS[2]
    ]);
  });
});

test.describe('Todo Counter', () => {
  test('should display the current number of todo items', async ({ page }) => {
    const todoInput = page.locator('.new-todo');
    const todoCounter = page.locator('.todo-count');

    await todoInput.fill(TODO_ITEMS[0]);
    await todoInput.press('Enter');
    await expect(todoCounter).toContainText('1');

    await todoInput.fill(TODO_ITEMS[1]);
    await todoInput.press('Enter');
    await expect(todoCounter).toContainText('2');

    // Mark the first one as completed.
    page.locator('.todo-list li').nth(0).locator('.toggle').check();

    await expect(todoCounter).toContainText('1');
  });
});
